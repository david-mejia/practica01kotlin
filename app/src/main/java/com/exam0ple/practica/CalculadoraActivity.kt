package com.exam0ple.practica

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class CalculadoraActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculadora)

        val lblNombre = findViewById<TextView>(R.id.lblNombre)
        val txtNum1 = findViewById<EditText>(R.id.txtNum1)
        val txtNum2 = findViewById<EditText>(R.id.txtNum2)
        val btnSuma = findViewById<Button>(R.id.btnSuma)
        val btnResta = findViewById<Button>(R.id.btnResta)
        val btnMultiplicacion = findViewById<Button>(R.id.btnMultiplicacion)
        val btnDivision = findViewById<Button>(R.id.btnDivision)
        val btnLimpiar = findViewById<Button>(R.id.btnLimpiar)
        val btnRegresar = findViewById<Button>(R.id.btnRegresar)

        // Mostrar el nombre desde el recurso de strings
        lblNombre.text = getString(R.string.nombre)

        val calculadora = Calculadora()

        btnSuma.setOnClickListener {
            val num1 = txtNum1.text.toString().trim()
            val num2 = txtNum2.text.toString().trim()
            if (validarEntradas(num1, num2)) {
                val resultado = calculadora.suma(num1.toFloat(), num2.toFloat())
                showResultDialog(resultado)
            }
        }

        btnResta.setOnClickListener {
            val num1 = txtNum1.text.toString().trim()
            val num2 = txtNum2.text.toString().trim()
            if (validarEntradas(num1, num2)) {
                val resultado = calculadora.resta(num1.toFloat(), num2.toFloat())
                showResultDialog(resultado)
            }
        }

        btnMultiplicacion.setOnClickListener {
            val num1 = txtNum1.text.toString().trim()
            val num2 = txtNum2.text.toString().trim()
            if (validarEntradas(num1, num2)) {
                val resultado = calculadora.multiplicacion(num1.toFloat(), num2.toFloat())
                showResultDialog(resultado)
            }
        }

        btnDivision.setOnClickListener {
            val num1 = txtNum1.text.toString().trim()
            val num2 = txtNum2.text.toString().trim()
            if (validarEntradas(num1, num2)) {
                try {
                    val resultado = calculadora.division(num1.toFloat(), num2.toFloat())
                    showResultDialog(resultado)
                } catch (e: IllegalArgumentException) {
                    showErrorDialog(e.message.toString())
                }
            }
        }

        btnLimpiar.setOnClickListener {
            txtNum1.text.clear()
            txtNum2.text.clear()
        }

        btnRegresar.setOnClickListener {
            finish()
        }
    }

    private fun validarEntradas(num1: String, num2: String): Boolean {
        if (num1.isEmpty()) {
            Toast.makeText(this, "Ingrese el primer número", Toast.LENGTH_SHORT).show()
            return false
        }

        if (num2.isEmpty()) {
            Toast.makeText(this, "Ingrese el segundo número", Toast.LENGTH_SHORT).show()
            return false
        }

        try {
            num1.toFloat()
        } catch (e: NumberFormatException) {
            Toast.makeText(this, "El primer número no es válido", Toast.LENGTH_SHORT).show()
            return false
        }

        try {
            num2.toFloat()
        } catch (e: NumberFormatException) {
            Toast.makeText(this, "El segundo número no es válido", Toast.LENGTH_SHORT).show()
            return false
        }

        return true
    }

    private fun showResultDialog(resultado: Float) {
        AlertDialog.Builder(this)
            .setTitle("Resultado")
            .setMessage(getString(R.string.calculation_result, resultado.toString()))
            .setPositiveButton("OK", null)
            .show()
    }

    private fun showErrorDialog(message: String) {
        AlertDialog.Builder(this)
            .setTitle("Error")
            .setMessage(message)
            .setPositiveButton("OK", null)
            .show()
    }
}
