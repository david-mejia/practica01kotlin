package com.exam0ple.practica

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val txtUsuario = findViewById<EditText>(R.id.txtUsuario)
        val txtContraseña = findViewById<EditText>(R.id.txtContraseña)
        val btnIngresar = findViewById<Button>(R.id.btnIngresar)
        val btnSalir = findViewById<Button>(R.id.btnSalir)

        btnIngresar.setOnClickListener {
            val usuario = txtUsuario.text.toString().trim()
            val contraseña = txtContraseña.text.toString().trim()

            if (usuario.isEmpty()) {
                txtUsuario.error = "Ingrese el usuario"
                return@setOnClickListener
            }

            if (contraseña.isEmpty()) {
                txtContraseña.error = "Ingrese la contraseña"
                return@setOnClickListener
            }

            if (usuario == getString(R.string.user) && contraseña == getString(R.string.pass)) {
                val intent = Intent(this, CalculadoraActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show()
            }
        }

        btnSalir.setOnClickListener {
            finish()
        }
    }
}
