package com.exam0ple.practica

class Calculadora {
    fun suma(num1: Float, num2: Float): Float {
        return num1 + num2
    }

    fun resta(num1: Float, num2: Float): Float {
        return num1 - num2
    }

    fun multiplicacion(num1: Float, num2: Float): Float {
        return num1 * num2
    }

    fun division(num1: Float, num2: Float): Float {
        if (num2 != 0f) {
            return num1 / num2
        } else {
            throw IllegalArgumentException("División por cero no permitida")
        }
    }
}

